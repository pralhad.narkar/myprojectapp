const products = [
    { 
      name: 'Corn & Cheese Pizza',
      image: '/images/d0bd7c9405ac87f6aa65e31fe55800941632716575.avif',
      description:
        'Earlier known as American Cheese Supreme Veg, this is a burger with crispy corn & cheese patty, covered with a slice of cheese, creamy cocktail sauce, jalapenos and shredded onions.',
      brand: 'Burberry',
      category: 'Pizza',
      price: 175,
      countInStock: 22,
      rating: 2.6,
      numReviews: 4,
    },
    { 
      name: 'Cheez Burger',
      image: '/images/pexels-rajesh-tp-1633578.jpg',
      description:
        'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
      brand: 'Urban Outfitters',
      category: 'Burger',
      price: 8500,
      countInStock: 42,
      rating: 4.4,
      numReviews: 2,
    },
 { 
name: 'perri perri burger',
      image: '/images/pizza-g87c380c3c_1920.jpg',
      description:
        'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
      brand: 'Ralph Lauren',
      category: 'Burger',
      price: 6000,
      countInStock: 18,
      rating: 4.5,
      numReviews: 1,
    },
    { 
      name: 'French Fries',
      image: '/images/pexels-photo-115740.jpeg',
      description:
        'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
      brand: 'Gucci',
      category: 'Fries',
      price: 21000,
      countInStock: 8,
      rating: 4.8,
      numReviews: 12,
    },
    { 
      name: 'Perri Perri Pizza',
      image: '/images/pexels-photo-4553111.jpeg',
      description:
        'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
      brand: 'Chanel',
      category: 'Pizza',
      price: 43000,
      countInStock: 6,
      rating: 4.9,
      numReviews: 8,
    },
    {
      name: 'Cheez Pizza',
      image: '/images/svd.jpg',
      description:
        'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
      brand: 'Tom Ford',
      category: 'Pizza',
      price: 16000,
      countInStock: 21,
      rating: 4.1,
      numReviews: 3,
    },
    { 
      name: 'Tomato Stuff pizza',
      image: '/images/pizza-2068272__340.jpg',
      description:
        'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
      brand: 'Calvin Klein',
      category: 'Pizza',
      price: 7500,
      countInStock: 25,
      rating: 3.4,
      numReviews: 3,
    },
    { 
      name: 'Margaritta Pizza',
      image: '/images/pizza-3007395__340.jpg',
      description:
        'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
      brand: 'Chanel',
      category: 'Pizza',
      price: 9500,
      countInStock: 0,
      rating: 4.2,
      numReviews: 5,
    },
  ];
  
  export default products;
  