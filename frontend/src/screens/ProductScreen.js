import { Flex, Grid, Image, Heading, Text, Button, Select, Box,FormLabel,FormControl,Link, Textarea } from '@chakra-ui/react';
import { Link as RouterLink, useNavigate, useParams} from 'react-router-dom';
import Rating from '../components/Rating';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { createProductReview, listProductDetails } from '../actions/productAction';
import { PRODUCT_REVIEW_CREATE_RESET } from '../constants/productConstants';

const ProductScreen = () => {
    const { id } = useParams();
    
    const dispatch = useDispatch();
    const navigate = useNavigate();

	const [qty, setQty] = useState(1);
	const [rating, setRating] = useState(0)
	const [comment, setComment] = useState('')

    const productDetails = useSelector((state) => state.productDetails);
    const { loading, error, product} = productDetails;

	const productReviewCreate = useSelector((state) => state.productReviewCreate);
	const { success:successProductReview, error:errorProductReview} = productReviewCreate;

	const userLogin = useSelector((state) => state.userLogin);
	const {userInfo} = userLogin;

	const submitHandler = (e) => {
	e.preventDefault()
	dispatch(createProductReview(id, {rating, comment}))
	}




    useEffect(() => {
		if(successProductReview){
			alert('review submitted');
			setRating(0);
			setComment('');
			dispatch({ type:PRODUCT_REVIEW_CREATE_RESET})
		}
        dispatch(listProductDetails(id));
         },[id, dispatch, successProductReview])

     const addToCartHandler = () => {
        navigate(`/cart/${id}?qty=${qty}`)
     }
     return (
		<>
			<Flex mb='5'>
				<Button as={RouterLink} to='/' colorScheme='purple'>
					Go Back
				</Button>
			</Flex>

			{loading ? (
				<Loader />
			) : error ? (
				<Message type='error'>{error}</Message>
			) : (
				<>
				<Grid templateColumns='4fr 4fr' gap='10'>
					{/* Column 1 */}
					<Image src={product.image} alt={product.name} borderRadius='md' h='400px' />

					{/* Column 2 */}
					<Flex direction='column'>
						<Heading as='h6' fontSize='base' color='gray.500'>
							{product.brand}
						</Heading>
						<Heading as='h2' fontSize='4xl' mb='2' color='gray.300'>
							{product.name}
						</Heading>
						<Rating
							value={product.rating}
							text={`${product.numReviews} reviews`}
						/>
						<Heading
							as='h5'
							my='5'
							fontWeight='bold'
							fontSize='4xl'
							color='teal.600'>
							₹{product.price}
						</Heading>
						<Text color='gray.300'>{product.description}</Text>
					</Flex>

					{/* Column 3 */}
					<Flex direction='column' color='gray.300'>
						<Flex justifyContent='space-between' py='2'>
							<Text>Price: </Text>
							<Text fontWeight='bold'>₹{product.price}</Text>
						</Flex>
						<Flex justifyContent='space-between' py='2'>
							<Text>Status: </Text>
							<Text fontWeight='bold'>
								{product.countInStock > 0 ? 'In stock' : 'Not available'}
							</Text>
						</Flex>

                        {product.countInStock > 0 && (
							<Flex justifyContent='space-between' py='2' color='gray.300' >
								<Text>Qty: </Text>
								<Select
									value={qty}
									onChange={(e) => setQty(e.target.value)}
									width='30%'
									>
									{[...Array(product.countInStock).keys()].map((i) => (
										<option key={i + 1} value={i + 1}>
											{i + 1}
										</option>
									))}
								</Select>
							</Flex>
						)}
						
						<Button
							// bgColor='gray.800'
							textTransform='uppercase'
							letterSpacing='wide'
							colorScheme='purple'
							size='md'
							variant='solid'
							
							alignItems='center'
							justifyContent='center'
							my='2'
							disabled={product.countInStock === 0}
							onClick={addToCartHandler}>
							Add To Cart
						</Button>
					
					</Flex>
				</Grid>
				{/* Review Form */}
				<Box mt='10'>
				<Box>
					<Heading as='h2' size='xl' mb='4'>
						Reviews
					</Heading>

					{product.reviews.length === 0 && <Message>No Reviews</Message>}

					{product.reviews.length !== 0 && (
						<Box p='4' borderColor='gray.300' border='1px' borderRadius='md' color='gray.300' rounded='mb' mb='1' mt='5'>
							{product.reviews.map((review) => (
								<Flex direction='column' key={review._id} mb='5'>
									<Flex justifyContent='space-between'>
										<Text fontSize='lg'>
											<strong>{review.name}</strong>
											{review.createdAt?.substring(0, 10)}
										</Text>
										<Rating value={review.rating} />
									</Flex>
									<Text mt='2'>{review.comment}</Text>
								</Flex>
							))}
						</Box>
					)}

					{/* Form */}
					<Box
						p='10'
						bgColor='black'
						rounded='md'
						border='1px'
						mt='10'
						color='white'
						borderColor='gray.300'>
						<Heading as='h3' size='lg' mb='6'>
							Write a review
						</Heading>

						{errorProductReview && (
							<Message type='error'>{errorProductReview}</Message>
						)}

						{userInfo ? (
							<form onSubmit={submitHandler}>
								<FormControl id='rating' mb='3' color='gray.300'>
									<FormLabel>Rating</FormLabel>
									<Select
										placeholder='Select Option'
										value={rating}
										onChange={(e) => setRating(e.target.value)}>
										<option>Select...</option>
										<option value='1'>1 - Poor</option>
										<option value='2'>2 - Okay</option>
										<option value='3'>3 - Good</option>
										<option value='4'>4 - Very Good</option>
										<option value='5'>5 - Excellent</option>
									</Select>
								</FormControl>

								<FormControl id='comment' mb='3'>
									<FormLabel>Comment</FormLabel>
									<Textarea
										value={comment}
										onChange={(e) => setComment(e.target.value)}></Textarea>
								</FormControl>

								<Button colorScheme='teal' type='submit'>
									Post Review
								</Button>
							</form>
						) : (
							<Message>
								Please{' '}
								<Link as={RouterLink} to='/login'>
									login
								</Link>{' '}
								to write a review
							</Message>
						)}
					</Box>
				</Box>
			</Box>
		</>
	)}
		</>
	);
};

export default ProductScreen;
