import { Heading, Grid, Flex , Image, Link} from "@chakra-ui/react";
import Product from "../components/Product";
import { useEffect  } from "react";
import {  listProducts } from "../actions/productAction";
import { useDispatch, useSelector } from "react-redux";
import Loader from '../components/Loader'
import Message from "../components/Message";
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination, Navigation,FreeMode,Mousewheel, Keyboard ,EffectCreative} from "swiper";
import { EffectFade } from 'swiper';
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import {Link as RouterLink} from 'react-router-dom'

const HomeScreen = () => {
    const dispatch = useDispatch();

    const productList = useSelector((state) => state.productList);
    const { loading, products, error} = productList;

    useEffect(() => {
        dispatch(listProducts())
    },[dispatch])

    return (
        <div>
            <Flex justify='center' bg='black' borderRadiouse
            ='md'>
            <Heading as = 'h2' mb='4' fontSize='3xl' color='yellow' textShadow='lg' border='1px' textIndent='revert' p='2' >
                Grab the deal
            </Heading>
            </Flex>
            <Flex bg='black'>
            <Swiper
          // mt='100px' 
          speed={1000}
        // spaceBetween={10}
        
        centeredSlides={false}
     
        autoplay={{
          delay: 4500,
          
          disableOnInteraction: false,
          transition:EffectFade
          
        }}
        pagination={{
          clickable: true,
        }}
        navigation={false}
        modules={[Autoplay, Pagination, Navigation,EffectCreative]}
        className="mySwiper"
        
      >
      <SwiperSlide><Link as = {RouterLink} to = {'/product/64bfcb0044a575221586c2ac'}><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/make-a-food-combo-by-combining-high-and-low-performing-items.jpg" w='70%' h='500px'/></Flex></Link> </SwiperSlide>
      <SwiperSlide><Link as={RouterLink} to = {'/product/64bfcb0044a575221586c2aa'}><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/create-a-complete-2-through-half-and-half.jpg" w='70%'h='500px'/></Flex></Link></SwiperSlide>
      <SwiperSlide><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/promote-your-food-combo-offers.jpg" w='70%'h='500px'/></Flex></SwiperSlide>
      <SwiperSlide><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/add-drinks-to-make-a-delightful-food-combo.jpg" w='70%'h='500px'/></Flex></SwiperSlide>
      <SwiperSlide><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/allow-customize-combo-depending-on-your-customers-choice.jpg" w='70%'h='500px'/></Flex></SwiperSlide>
      <SwiperSlide><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/offer-combo-food-based-on-the-season.jpg" w='70%'h='500px'/></Flex></SwiperSlide>
      <SwiperSlide><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/restaurant-food-combo-offers.jpg" w='70%'h='500px'/></Flex></SwiperSlide>
      <SwiperSlide><Flex w='100%'  h='400px' justifySelf='center' alignItems='center' justifyContent='center'> <Image src="/images/categorize-your-food-combo.jpg" w='70%'h='500px'/></Flex></SwiperSlide>

      ...
    </Swiper>
            </Flex>
            <Heading as = 'h2' mb='8' fontSize='3xl'>
                Select Your Food
            </Heading>
            {loading ? (
                <Loader/>
            ) : error ? (
                <Message type = 'error'>{error}</Message>
            ) : (
                <Grid templateColumns={{
                    sm: '1fr',
                    md: '1fr 1fr',
                    lg: '1fr 1fr 1fr',
                    xl: '1fr 1fr 1fr 1fr'
    
                }} gap='8'>
    
                    {products.map((prod) => (
                        <Product key={prod._id} product={prod}/>
                    ))}
                </Grid>
            )}
            
        </div>
    )
}

export default HomeScreen;