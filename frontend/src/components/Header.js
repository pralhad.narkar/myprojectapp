import { useState } from 'react';
import { Flex, Heading, Link, Box, Icon, Menu,MenuList,Button,MenuButton, MenuItem } from '@chakra-ui/react';
import { HiShoppingBag, HiUser, HiOutlineMenuAlt3 } from 'react-icons/hi';
import { Link as RouterLink, useNavigate} from 'react-router-dom';
import { IoCheckbox, IoChevronBack, IoChevronDown } from 'react-icons/io5';
import { useSelector , useDispatch} from 'react-redux';
import {logout} from '../actions/userAction'



const Header = () => {
    const [show, setShow] = useState(false);

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const userLogin = useSelector((state) => state.userLogin)
    const { userInfo} = userLogin;

    const logoutHandler = () => {
      dispatch(logout())
      navigate('/')
    }


    return(
        <Flex 
        as = 'header'
        align='center'
        opancity='0.2'
        justifyContent='space-between'
        wrap='wrap'
        py='6'
        px='6'
        bgColor='yellow'
        bgGradient="linear(to-l, #ebeb34, #FF0080)"
        w='100%'
        pos='fixed'
        top='0'
        zIndex='99999'>
            <Heading
                as='h1'
                color='black'
                fontWeight='bold'
                fontFamily='san-sarif'
                size='lg'
                letterSpacing='md'>
                    <Link as= {RouterLink} to= '/' _hover={{color: 'yellow', textDecor: 'none'}}>
                        My Store
                    </Link>
            </Heading>
            <Box
            display={{base:'block',md:'none'}}
            onClick={() => setShow(!show)}>
                <Icon as ={HiOutlineMenuAlt3} w='6' h='6' color='black'/>
            </Box>
            <Box
            
            display={{ base :show? 'block' : 'none', md:'flex'}}
            width={{ base: 'full', md: 'auto'}}
            mt={{ base :'3',md:'0'}}>
             <Link
          as = {RouterLink}  
          borderRadius='md' 
          to='/cart'
          fontSize='sm'
          letterSpacing='wide'
          color='black'
          textTransform='uppercase'
          mr='5'
          display='flex'
          fontWeight='semibold'
          alignItems='center'
          _hover={{ color: 'red' }}
          border='1px'
          px ='5px'
          
        >
          <Icon as={HiShoppingBag} w='4' h='4' mr='1' />
          Cart
        </Link>

        {userInfo ? (

          <Menu>
            <MenuButton
            as={Button}
            rightIcon={<IoChevronDown/>}
            _hovert={{ textDecor: 'none',  color :'red'}}
            color='black'
            border='1px'
            alignItems='center'
            >
              {userInfo.name}
            </MenuButton>
            <MenuList>
              <MenuItem as={RouterLink} to='/profile'>
                Profile
              </MenuItem>
              <MenuItem onClick={logoutHandler}>Logout</MenuItem>
            </MenuList>

          </Menu>
        ) : (
          <Link
          as = { RouterLink}
          to='/login'
          fontSize='sm'
          letterSpacing='wide'
          color='black'
          textTransform='uppercase'
          mr='5'
          borderRadius='md'
          border='1px'
          p='1'
          display='flex'
          fontWeight='bold'
          alignItems='center'
          _hover={{ color: 'red' }}
        >
          <Icon as={HiUser} w='4' h='4' mr='1' />
          Login
        </Link>   
        )}
        
                {/* Admin Menu */}
          {userInfo && userInfo.isAdmin && (
            <Menu>
              <MenuButton
              ml='5'
              p='2'
              borderRadius='md'
              color='black'
              fontSize='sm'
              fontWeight='semibold'
              as={Link}
              
              border='1px'
              justifyContent='flex-end' alignItems='center'
              textTransform='uppercase'
              _hover={{ textDecoration: 'none', color:'red' }}>
              Manage <Icon as={IoChevronDown} />
              </MenuButton>
              <MenuList>
              <MenuItem as={RouterLink} to='/admin/userlist'>
                All Users
              </MenuItem>
              <MenuItem as={RouterLink} to='/admin/productlist'>
                All Products
              </MenuItem>
              <MenuItem as={RouterLink} to='/admin/orderlist'>
                All Orders
              </MenuItem>
              </MenuList>
            </Menu>
          )}

         </Box>
        </Flex>
    )
}

export default Header;